import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';
import jwt_decode from "jwt-decode";

Vue.use(Vuex)
const apiUrl = 'http://localhost:8000';
export default new Vuex.Store({
  state: {
    user: null

  },
  mutations: {
    CHANGE_USER(state, user) {
      state.user = user;
    },

  },
  actions: {
    changeUser({ commit }, user) {
      //On ne peut commit que les nom de méthodes qui existent dans mutations
      commit('CHANGE_USER', user);

    },
    async login({ commit }, credentials) {
      //L'action va d'abord faire un appel ajax faire l'api rest
      let response = await Axios.post(apiUrl+'/api/login_check', credentials);
      //Si ça marche, on stock le token dans le localStorage
      localStorage.setItem('token', response.data.token);
      //Et on lance un commit pour changer l'état de notre store
      commit('CHANGE_USER', credentials.username);
    },
    logout({ commit }) {
      //A la déconnexion, on dégage le token du localStorage
      localStorage.removeItem('token');
      //Et on change l'état de notre user dans le store
      commit('CHANGE_USER', null);
    },
    checkToken({ commit }) {
      if (localStorage.getItem('token')) {
        let token = jwt_decode(localStorage.getItem('token'));
        if (new Date().getTime() < token.exp * 1000) {
          commit('CHANGE_USER', token.username);
        }else {
          localStorage.removeItem('token');
        }
      }
    },

  }
})
